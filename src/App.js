import { useState } from "react";
import "./App.css"

function App() {
  const [name, setName] = useState();
  const [job, setJob] = useState();
  const [age, setAge] = useState();
  const [location, setLocation] = useState();
  const [details, setDetails] = useState({
    name: "kaz",
    job: "developer",
    age: 20,
    location: "london",
  });

  const getName = (e) => {
    setName(e.target.value);
  };
  const getJob = (e) => {
    setJob(e.target.value);
  };
  const getAge = (e) => {
    setAge(e.target.value);
  };
  const getLocation = (e) => {
    setLocation(e.target.value);
  };
  const gatherDetails = async () => {
    setDetails({ name: name, job: job, age: Number(age), location: location });
    const post = await fetch(
      `http://a9de577220633447cadb3344a0bcd287-1512421268.eu-west-2.elb.amazonaws.com:3001/users`,
      {
        method: "POST",

        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(details),
      }
    );
    const dat = await post.json();
    if(dat){
      alert("info submited")
    }
    console.log(dat);
  };
  return (
    <div className="App">
      <h1>Mobilise Details Collectors App</h1>
      <label className="label1">
        {" "}
        Name:
      </label>
        <input className="input" onChange={getName}></input>
      <br></br>
      <label className="label2">
        {" "}
        Age:
      </label>
        <input className="input" type="number" onChange={getAge}></input>
      <br></br>
      <label className="label3">
        {" "}
        Job:
      </label>
        <input className="input" onChange={getJob}></input>
      <br></br>
      <label className="label4">
        {" "}
        Location:
      </label>
        <input  className="input" onChange={getLocation}></input>
      <br></br>
      <input className="submit" type="submit" value="Submit" onClick={gatherDetails} />
    </div>
  );
}

export default App;
