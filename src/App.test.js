import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders learn react link", () => {
  render(<App />);
  const header = screen.getByText("Mobilise Details Collectors App");
  expect(header).toBeInTheDocument();
});
test("label in the doc", () => {
  render(<App />);
  const header = screen.getByText("Name:");
  expect(header).toBeInTheDocument();
});
test("label2 in the doc", () => {
  render(<App />);
  const header = screen.getByText("Job:");
  expect(header).toBeInTheDocument();
});
test("label3 in the doc", () => {
  render(<App />);
  const header = screen.getByText("Location:");
  expect(header).toBeInTheDocument();
});
