def test(){
    sh "npm ci --production"
    sh "npm run test"
}

def bumpversion(String bump){
    sh 'git config --global user.email "jenkins@example.com"'
    sh 'git config --global user.name "jenkins"'
    sh "npm version $bump"
    def packageJson = readJSON file: 'package.json'
    env.version = packageJson.version
}


def builddocker(){
    withCredentials([usernamePassword(credentialsId: 'AWS_LOGIN', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t 063942655453.dkr.ecr.eu-west-2.amazonaws.com/mobilise-frontend:$version ."
        sh "echo $PASS | docker login --username $USER --password-stdin 063942655453.dkr.ecr.eu-west-2.amazonaws.com"
        sh "docker push 063942655453.dkr.ecr.eu-west-2.amazonaws.com/mobilise-frontend:$version"
        sh "docker rmi 063942655453.dkr.ecr.eu-west-2.amazonaws.com/mobilise-frontend:$version"
    }
}

def commitgit(){
    withCredentials([usernamePassword(credentialsId: 'Gitlab', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
sh "git remote set-url origin https://$USER:$PASS@gitlab.com/cazmaar/mobilise-frontend.git"
sh "git push origin HEAD:master"
    // sh "echo $PASS"
    }
}



// def deploy(){
//     echo "app deployed."
// }

return this